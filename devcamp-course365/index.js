const express = require('express');
const app = express();
const port = 8000;
const path = require('path');

//Import mongoose
var mongoose = require('mongoose');
var uri = "mongodb://localhost:27017/CRUD_Course365";

// Cấu hình để app đọc được body request dạng json
app.use(express.json());

// Cấu hình để app được được tiếng Việt UTF8
app.use(express.urlencoded({
    extended: true
}))

//Import Model để sinh ra DB
const courseModel = require('./app/models/courseModel');

//Import router
const courseRouter = require('./app/routes/courseRouter');

app.use(express.static(`views/Course_365`)); // Use this for show image

app.get("/course-365", (request, response) => { // Show trang chủ course 365
  console.log(`__dirname: ${__dirname}`);
  response.sendFile(path.join(`${__dirname}/views/Course_365/course_index.html`));
})

app.use('/', courseRouter); // tất cả request về course đều chạy ở router này

//Connect DB
mongoose.connect(uri, function (error) {
	if (error) throw error;
	console.log('MongoDB Successfully connected');
});

app.listen(port, () => {
  console.log(`7.40 app listening on port ${port}`);
})
