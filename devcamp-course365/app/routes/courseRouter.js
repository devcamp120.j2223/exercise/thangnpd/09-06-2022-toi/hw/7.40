//Import router
const express = require('express');
const router = express.Router();

//Import Middlewares
const {courseMiddleware} = require('../middlewares/courseMiddlewares');

//Import Controller
const { createCourse, 
        getAllCourse, 
        getCourseById,
        updateCourseById,
        deleteCourseById
      } = require('../controllers/courseController');

//Create new course
router.post('/courses', courseMiddleware, createCourse);

//Get all courses
router.get('/courses', courseMiddleware, getAllCourse);

//Get 1 course by ID
router.get('/courses/:courseId', courseMiddleware, getCourseById);

//Update 1 course by ID
router.put('/courses/:courseId', courseMiddleware, updateCourseById);

//Delete 1 course by ID
router.delete('/courses/:courseId', courseMiddleware, deleteCourseById);

module.exports = router; //Export router để call main app ở file index.js