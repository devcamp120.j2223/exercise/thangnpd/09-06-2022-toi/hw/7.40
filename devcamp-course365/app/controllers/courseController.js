//Import course model vào để tương tác với model
const courseModel = require('../models/courseModel');

//Khai báo thư viện mongoose để tạo _id cho mỗi record mới
var mongoose = require('mongoose');

//Hàm create course
const createCourse = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;

  // B2: Kiểm tra dữ liệu
  if (!bodyRequest.courseCode) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "courseCode is required"
    })
  }
  if (!bodyRequest.courseName) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "courseName is required"
    })
  }
  if (!(Number.isInteger(bodyRequest.price) && bodyRequest.price > 0)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "price is not valid"
    })
  }
  if (!(Number.isInteger(bodyRequest.discountPrice) && bodyRequest.discountPrice > 0)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "discountPrice is not valid"
    })
  }
  if (!bodyRequest.duration) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "duration is required"
    })
  }
  if (!bodyRequest.level) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "level is required"
    })
  }
  if (!bodyRequest.coverImage) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "coverImage is required"
    })
  }
  if (!bodyRequest.teacherName) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "teacherName is required"
    })
  }
  if (!bodyRequest.teacherPhoto) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "teacherPhoto is required"
    })
  }
  // B3: Thao tác với cơ sở dữ liệu
  let createCourse = {
    _id: new mongoose.Types.ObjectId(),
    courseCode: bodyRequest.courseCode,
    courseName: bodyRequest.courseName,
    price: bodyRequest.price,
    discountPrice: bodyRequest.discountPrice,
    duration: bodyRequest.duration,
    level: bodyRequest.level,
    coverImage: bodyRequest.coverImage,
    teacherName: bodyRequest.teacherName,
    teacherPhoto: bodyRequest.teacherPhoto,
    isPopular: bodyRequest.isPopular,
    isTrending: bodyRequest.isTrending,
    ngayTao: bodyRequest.ngayTao,
    ngayCapNhat: bodyRequest.ngayCapNhat
  }
  courseModel.create(createCourse, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(201).json({
        status: "Success: Course created",
        data: data
      })
    }
  })
}
//Hàm get all courses
const getAllCourse = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  courseModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get all courses success",
        data: data
      })
    }
  })
}
//Hàm get course by ID
const getCourseById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let courseId = request.params.courseId;
  console.log(courseId);
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Course ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  courseModel.findById(courseId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get course by ID success",
        data: data
      })
    }
  })
}
//Hàm update course by ID
const updateCourseById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let courseId = request.params.courseId;
  let bodyRequest = request.body;

  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Course ID is not valid"
    })
  }

  //B3: Thao tác với cơ sở dữ liệu
  let courseUpdate = {
    courseName: bodyRequest.courseName,
    price: bodyRequest.price,
    discountPrice: bodyRequest.discountPrice,
    duration: bodyRequest.duration,
    level: bodyRequest.level,
    coverImage: bodyRequest.coverImage,
    teacherName: bodyRequest.teacherName,
    teacherPhoto: bodyRequest.teacherPhoto,
    isPopular: bodyRequest.isPopular,
    isTrending: bodyRequest.isTrending
  }
  courseModel.findByIdAndUpdate(courseId, courseUpdate, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update course success",
        data: data
      })
    }
  })
}

//Hàm delete course by ID
const deleteCourseById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let courseId = request.params.courseId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Course ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  courseModel.findByIdAndDelete(courseId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(204).json({
        status: "Success: Delete course success"
      })
    }
  })
}
module.exports = {
  createCourse,
  getAllCourse,
  getCourseById,
  updateCourseById,
  deleteCourseById
}